package com.wanchalerm.tua.common.logging.filter

import com.fasterxml.jackson.databind.ObjectMapper
import com.wanchalerm.tua.common.extension.LoggerDelegate
import com.wanchalerm.tua.common.extension.hiddenJsonValue
import com.wanchalerm.tua.common.extension.maskJsonValue
import com.wanchalerm.tua.common.extension.maskSensitiveData
import com.wanchalerm.tua.common.logging.config.MaskingConfig
import jakarta.servlet.FilterChain
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.springframework.stereotype.Component
import org.springframework.util.StopWatch
import org.springframework.web.filter.OncePerRequestFilter
import org.springframework.web.util.ContentCachingResponseWrapper
import java.io.IOException


@Component
class LoggingFilter(private val maskingConfig: MaskingConfig) : OncePerRequestFilter() {
    private val log by LoggerDelegate()
    private val objectMapper = ObjectMapper()

    init {
        maskingConfig.hiddenKeys.addAll(listOf("password", "passcode", "pass", "key"))
    }

    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        val requestWrapper = RepeatableContentCachingRequestWrapper(request)
        val responseWrapper = ContentCachingResponseWrapper(response)
        logRequest(requestWrapper)
        val stopWatch = StopWatch()
        stopWatch.start()
        filterChain.doFilter(requestWrapper, responseWrapper)
        stopWatch.stop()
        logResponse(requestWrapper, responseWrapper, stopWatch)
    }

    @Throws(IOException::class)
    private fun logRequest(requestWrapper: RepeatableContentCachingRequestWrapper) {
        val body = requestWrapper.readInputAndDuplicate()
            .maskJsonValue(maskingConfig.maskingKeys)
            .hiddenJsonValue(maskingConfig.hiddenKeys)
        val params = getAllParam(requestWrapper)

        log.info(
            buildString {
                append("[REQUEST]")
                append("method=[{}]")
                append("path=[{}]")
                append("param={}")
                append("body=[{}]")
            },
            requestWrapper.method,
            requestWrapper.requestURI,
            objectMapper.writeValueAsString(params),
            body
        )
    }

    @Throws(IOException::class)
    private fun logResponse(
        requestWrapper: RepeatableContentCachingRequestWrapper,
        responseWrapper: ContentCachingResponseWrapper,
        stopWatch: StopWatch
    ) {
        val body = String(responseWrapper.contentAsByteArray)
            .maskJsonValue(maskingConfig.maskingKeys)
            .hiddenJsonValue(maskingConfig.hiddenKeys)
            .maskSensitiveData()

        log.info(
            buildString {
                append("[RESPONSE]")
                append("timeUsage=[{}] ms")
                append("method=[{}]")
                append("path=[{}]")
                append("status=[{}]")
                append("body=[{}]")
            },
            stopWatch.totalTimeMillis,
            requestWrapper.method,
            requestWrapper.requestURI,
            responseWrapper.status,
            body
        )
        responseWrapper.copyBodyToResponse()
    }

    private fun getAllParam(request: RepeatableContentCachingRequestWrapper) =
        request.parameterNames
        .asSequence()
        .associateWith { request.getParameterValues(it).toList() }
}