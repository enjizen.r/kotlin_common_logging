package com.wanchalerm.tua.common.logging.filter

import com.wanchalerm.tua.common.constant.ThreadConstant.X_CLIENT_IP
import com.wanchalerm.tua.common.constant.ThreadConstant.X_CORRELATION_ID
import com.wanchalerm.tua.common.constant.ThreadConstant.X_DEVICE_ID
import com.wanchalerm.tua.common.extension.createCorrelationId
import jakarta.servlet.FilterChain
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.slf4j.MDC
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter

@Component
class CorrelationIdFilter : OncePerRequestFilter() {
    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        try {
            request.populateMDC()
            filterChain.doFilter(request, response)
        } finally {
            MDC.clear() // Ensure MDC values are cleared to prevent memory leaks in multi-threaded environments.
        }
    }

    private fun HttpServletRequest.populateMDC() {
        val correlationId = getHeader(X_CORRELATION_ID)
            .takeIf { !it.isNullOrBlank() }
            ?: "".createCorrelationId()

        MDC.put(X_CORRELATION_ID, correlationId)
        MDC.put(X_CLIENT_IP, remoteAddr)
        MDC.put(X_DEVICE_ID, getHeader(X_DEVICE_ID) ?: "")
    }
}